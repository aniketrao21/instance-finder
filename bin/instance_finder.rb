#!/usr/bin/env ruby

require 'json-schema'
require 'json'
require 'pry-nav'

CIDR_IP = '0.0.0.0/0'.freeze
SSH_PORT = 22

def validate_input_files(instance_file_path, sg_file_path)
  puts 'Validating instances against schema'
  JSON::Validator.validate!('schemas/instance.json', instance_file_path)
  puts 'Validating security groups against schema'
  JSON::Validator.validate!('schemas/sg.json', sg_file_path)
rescue JSON::Schema::ValidationError => e
  puts e.message
  exit(1)
end

def validate_inbound_permissions(ip_permissions)
  ip_permissions.any? do |ip|
    ip['IpRanges'].any? { |i| i.dig('CidrIp') == CIDR_IP } && ip.dig('FromPort') != SSH_PORT
  end
end

def validate_outbound_permissions(ip_permissions)
  ip_permissions.any? do |ip|
    ip['IpRanges'].any? { |i| i.dig('CidrIp') == CIDR_IP }
  end
end

def get_public_security_groups(security_groups)
  puts 'Finding security groups which have public access'
  security_groups = security_groups['SecurityGroups']
  security_groups.collect do |sg|
    if sg.values_at('IpPermissions', 'IpPermissionsEgress') &&
      validate_inbound_permissions(sg['IpPermissions']) &&
      validate_outbound_permissions(sg['IpPermissionsEgress'])
      sg['GroupId']
    end
  end
end

def find_instance_by_group_ids(instances_data, group_ids)
  puts 'Finding instances which have public access'
  public_instances = []
  instances_data['Reservations'].each do |reservation|
    reservation['Instances'].each do |instance|
      instance['SecurityGroups'].each do |sg|
        public_instances.push(instance) if group_ids.include?(sg['GroupId'])
      end
    end
  end
  public_instances
end

def execute
  if ARGV[0] && ARGV[1]
    instance_filepath = ARGV[0]
    sg_filepath = ARGV[1]

    validate_input_files(instance_filepath, sg_filepath)

    instances_data = JSON.parse(File.open(instance_filepath, 'r').read)
    security_groups_data = JSON.parse(File.open(sg_filepath, 'r').read)

    security_groups_ids = get_public_security_groups(security_groups_data).compact
    public_instances = find_instance_by_group_ids(instances_data, security_groups_ids)

    if public_instances.empty?
      puts "No public instances found"
    else
      puts "Total #{public_instances.size} public instances found"
      public_instances.each do |instance|
        puts '1: InstanceID: ' + instance['InstanceId']
      end
    end
  else
    puts 'Please pass the two files as arguments'
  end
end


execute