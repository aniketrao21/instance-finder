require 'spec_helper'
require_relative '../bin/instance_finder'

RSpec.describe('Instance Finder') do
  describe '#validate_input_files' do
    context 'when given input files are invalid' do
      it 'should return error when instance file is invalid' do
        invalid_instance_filepath = 'spec/fixtures/invalid/instance.json'
        valid_sg_filepath = 'spec/fixtures/valid/sg.json'

        expect { validate_input_files(invalid_instance_filepath, valid_sg_filepath) }.to raise_error SystemExit
      end

      it 'should return error when sg file is invalid' do
        valid_instance_filepath = 'spec/fixtures/valid/instance.json'
        invalid_sg_filepath = 'spec/fixtures/invalid/sg.json'

        expect { validate_input_files(valid_instance_filepath, invalid_sg_filepath) }.to raise_error SystemExit
      end

      it 'should validate the json as per schema' do
        valid_instance_filepath = 'spec/fixtures/valid/instance.json'
        valid_sg_filepath = 'spec/fixtures/valid/sg.json'

        expect { validate_input_files(valid_instance_filepath, valid_sg_filepath) }.to output("Validating instances against schema\nValidating security groups against schema\n").to_stdout
      end
    end
  end

  describe '#execute' do
    context 'when given input files are not provided' do
      it 'should return error when instance file is invalid' do
        expect { execute }.to output("Please pass the two files as arguments\n").to_stdout
      end
    end
  end
end