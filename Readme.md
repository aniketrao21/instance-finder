# Instance Finder
This finds the public instances give an input of AWS instances based on given rules 

Criteria for public access
has public subnet
inbound rule from 0.0.0.0 on ports != 22
has public IP

### Usage

```
$ bin/instance_finder.rb <path_to_instances_json> <path_to_sg_json>
```

### Tests

```
$ rspec
```